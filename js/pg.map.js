var pg = pg || {};

pg.map = (function ($) {
    var map = {},
        marker = { setMap: function () {} },
        tracePath = { setMap: function () {} },

        getLatitude = function (latLng) {
            return latLng.k;
        },

        getLongitude = function (latLng) {
            return latLng.B;
        },

        addMarker = function (event) {
            removeMarker();
            showPoint(event, this);
            if ($('#map-canvas').hasClass('ctrl')) {
                addPoint();
                showTrace();
            }
        },

        removeMarker = function () {
            marker.setMap(null);
        },

        updateCoords = function (event) {
            if (event.latLng) {
                $('#pLatitude').val(getLatitude(event.latLng));
                $('#pLongitude').val(getLongitude(event.latLng));
                if ($('#pTrace option:selected').length === 1) {
                    $('#pTrace option:selected').val(getLatitude(event.latLng) + ',' + getLongitude(event.latLng));
                    showTrace();
                }
            }
        },

        addPoint = function () {
            var selectedElement = $('#pTrace option:selected'),
                coords = $('#pLatitude').val()+','+$('#pLongitude').val(),
                newPoint = $('<option>', { 
                    value: coords,
                    text : coords 
                });

            if ($('#pLatitude').val() === '' || $('#pLongitude').val() === '') {
                return;
            }

            $('#pLatitude,#pLongitude').val('');
            if ($('#pTrace option').length === 0 || $('#pTrace option:selected').length !== 1) {
                $('#pTrace').append(newPoint);
                removeMarker();
            } else if (selectedElement.length === 1) {
                selectedElement.after(newPoint).prop('selected', false)
                newPoint.prop('selected', true);
                showPoint();
            }
        },

        showPoint = function (event, map) {
            updateCoords(event);
            marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                draggable: true
            });
            google.maps.event.addListener(marker, 'drag', updateCoords);
        },

        selectPoint = function (element) {
            var coords = element.val().split(','),
                point = {
                    latLng: new google.maps.LatLng(coords[0], coords[1])
                };

            removeMarker();
            showPoint(point, map);
        },

        removePoint = function () {
            $('#pLatitude,#pLongitude').val('');
            removeMarker();
            if ($('#pTrace option:selected').length > 0) {
                $('#pTrace option:selected').remove();
            }
        },

        getTracePoints = function () {
            var tracePoints = [];

            $('#pTrace option').each(function () {
                var coords = $(this).val().split(',');

                tracePoints.push(new google.maps.LatLng(coords[0], coords[1]));
            });

            return tracePoints;
        },

        showTrace = function () {
            removeTrace();
            tracePath = new google.maps.Polyline({
                path: getTracePoints(),
                geodesic: true,
                strokeColor: '#333',
                strokeOpacity: 0.9,
                strokeWeight: 3
            });

            tracePath.setMap(map);
        },

        removeTrace = function () {
            tracePath.setMap(null);
        };

        createMap = function () {
            var mapOptions = {
                    center: new google.maps.LatLng(52.405560740609786, 16.965125122070312),
                    zoom: 13,
                    minZoom: 6,
                    overviewMapControl: false,
                    panControl: false,
                    streetViewControl: false,
                    mapTypeControl: true,
                    mapMaker: false
                };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            google.maps.event.addListener(map, 'click', addMarker);
        },

        encode = function () {
            return google.maps.geometry.encoding.encodePath(tracePath.getPath());
        },

        decode = function () {
            var key,
                decodedPath = google.maps.geometry.encoding.decodePath($('#pEncoded').val());

            $('#pTrace option').remove();
            for (key in decodedPath) {
                if (decodedPath.hasOwnProperty(key)) {
                    $('#pTrace').append($('<option>', { 
                        value: getLatitude(decodedPath[key]) + ',' + getLongitude(decodedPath[key]),
                        text: getLatitude(decodedPath[key]) + ',' + getLongitude(decodedPath[key])
                    }));
                    if (key === '0') {
                        map.setCenter(new google.maps.LatLng(getLatitude(decodedPath[key]), getLongitude(decodedPath[key])));
                    }
                }
            }
        },

        navigate = function (element) {
            var option,
                coords,
                optionSelected = $('#pTrace option:selected');
            if (element === 'next') {
                if (optionSelected.length === 1) {
                    optionSelected.prop('selected', false);
                    option = optionSelected.next();
                } else {
                    option = $('#pTrace option:first');
                }
            } else {
                if (optionSelected.length === 1) {
                    optionSelected.prop('selected', false);
                    option = optionSelected.prev();
                } else {
                    option = $('#pTrace option:last');
                }
            }

            option.prop('selected', true).trigger('click');
            if (option.length) {
                coords = option.val().split(',');
                map.setCenter(new google.maps.LatLng(coords[0], coords[1]));
            }
        },

        addListeners = function () {

            $('#pTrace').on('click', function () {
                var option = $('#pTrace option:selected');
                if (option.length === 1) {
                    selectPoint(option);
                }
            });

            $('#pUndo').on('click', function () {
                $('#pTrace option:last').remove();
                showTrace();
            })

            $('#pAdd').on('click', function () {
                addPoint();
                showTrace();
            });

            $('#pRemove').on('click', function () {
                removePoint();
                showTrace();
            });

            $('#pSave').on('click', function () {
                $('#pEncoded').val(encode()).trigger('change');
            });

            $('#pLoad').on('click', function () {
                decode();
                showTrace();
            });

            $('#pNext,#pPrev').on('click', function () {
                navigate($(this).attr('data-element'))
            });

            $(window).bind('keydown', function(event) {
                if (event.keyCode === 17) {
                    $('#map-canvas').addClass('ctrl');
                }
            });
            $(window).bind('keyup', function(event) {
                if (event.keyCode === 17) {
                    $('#map-canvas').removeClass('ctrl');
                }
            });
        };

    return {
        init: function () {
            createMap();
            addListeners();
        }
    }
})($);